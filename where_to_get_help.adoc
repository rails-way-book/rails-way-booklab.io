== Where To Get Help

One good thing about Ruby and its spinoffs is, Ruby is such a good language the people fall in love with it, so there is a large community of people who not only use this language for profession, but are in love with it. So getting help in Ruby and Ruby on Rails should not be difficult. These are some of the places where you can get help.

=== Ruby on Rails Mailing List

The official mailing list of Ruby on Rails exists here https://groups.google.com/forum/#!forum/rubyonrails-talk. Its a Google Group, if you have an email you can register yourself so that you can ask questions her, if you are good enough you may answer some.

=== Reddit

I feel reddit's Ruby on Rails group is good. You can know about latest Rails news here, post your doubts, blogs etc. You can see reddit's Rails group here https://www.reddit.com/r/rubyonrails/ and here https://www.reddit.com/r/rails/. Yup unfortunately there are two groups for the same topic.

=== Stack Overflow

Stack Overflow is known by programmers as the place to ask tech questions. Visit https://stackoverflow.com/, but I have never felt okay with this platform as it has too many rules which I feel is ultra restrictive.
