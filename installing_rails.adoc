== Installing Rails

I hope you have Ruby installed on your system, if yes, launch your terminal and type

----
$ gem install rails
----

This should throw out a bunch of stuff on terminal, and atlast you must see something like this:

----
Successfully installed rails-6.0.0
Parsing documentation for rails-6.0.0
Done installing documentation for rails after 0 seconds
----

which means you have installed Rails. Else run to Stack Overflow. Once intalled test it out by typing this:

----
$ rails -v
Rails 6.0.0
----

And for me it says I have Rails 6. So yaay! Rails is installed.


